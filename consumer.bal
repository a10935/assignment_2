import ballerina/kafka;

import ballerina/log;

kafka:ConsumerConfiguration consumerConfiguration = {
    bootstrapServers: "localhost:9092",
    groupId: "ABCompany-consumer-group",
    topics: ["add-consumer-with-account"],
    valueDeserializerType: kafka:DES_ABC,
    schemaRegistryUrl: "http://localhost:8081/"
};


//creating a listener

listener kafka:Consumer consumer = new(consumerConfiguration);

//creating a service

service KafkaService on consumer {
    resource function onMessage(kafka:Consumer consumer, kafka:ConsumerRecord[] records) {
        foreach var kafkaRecord in records {
            anydata value = kafkaRecord.value;
            if (value is kafka:AvroGenericRecord) {
                log:printInfo(value.toString());
            } else {
                log:printError("Invalid record type received.");
            }
        }
    }
}

