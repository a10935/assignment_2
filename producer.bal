
import ballerina/io;
import ballerina/kafka;

kafka:ProducerConfiguration producerConfiguration = {
    bootstrapServers: "localhost:9092",
    valueSerializerType: kafka:SER_ABCompany,
    schemaRegistryUrl: "http://localhost:8081"
};


//
kafka:Producer producer = new(producerConfiguration);


//record types
public type Account record {
    int accountNumber;
    float balance;
};
public type Customer record {
    string name;
    int age;
    Account account;
};

//the schema

string schema = "{\"type\" : \"record\"," +
  "\"namespace\" : \"Thisaru\"," +
  "\"name\" : \"person\"," +
  "\"fields\" : [" +
  "{ \"name\" : \"name\", \"type\" : \"string\" }," +
    "{ \"name\" : \"age\", \"type\" : \"int\" }," +
    "{ \"name\" : \"account\"," +
    "\"type\" : {" +
      "\"type\" : \"record\"," +
      "\"name\" : \"account\"," +
      "\"fields\" : [" +
        "{ \"name\" : \"accountNumber\", \"type\" : \"int\" }," +
        "{ \"name\" : \"balance\", \"type\" : \"double\" }" +
      "]}" +
    "}" +
  "]}";

//customer record we use kafka to sent

Account account = {
    accountNumber: 19930808,
    balance: 123.23
};
Customer customer = {
    name: "Lahiru Perera",
    age: 28,
    account: account
};


//record that constists of data

type ABCompany record {
    
};

kafka:ABCompany abcompany = {
    schemaString: schema,
    dataRecord: customer
};

// send the message using send() function in kafka:Producer.

var result = producer->send(abcompany, "add-customer-with-account");
    if (result is kafka:ProducerError) {
        io:println(result);
    } else {
        io:println("Successfuly sent");
    }
}
